-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 01, 2022 at 07:35 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daisywardrobr`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_data`
--

DROP TABLE IF EXISTS `product_data`;
CREATE TABLE IF NOT EXISTS `product_data` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `Pname` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `imgName` varchar(100) NOT NULL,
  `Price` int(100) NOT NULL,
  `pQuantity` int(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_data`
--

INSERT INTO `product_data` (`product_id`, `Pname`, `description`, `imgName`, `Price`, `pQuantity`, `type`) VALUES
(12, 'Frock', 'Black party frock .we have in all sizes Small, Medium ,Large', 'wwwww.jpg', 3500, 5, 'ladies'),
(13, 'office clothing', 'ladies office clothing full suit.We have in all sizes Small, Medium ,Large', 'Office-blouses.jpg', 5000, 5, 'ladies'),
(11, 'shoes', 'black shoes', '9-500-corsac-black-original-imagbyzezm5vkhyf-bb.jpeg', 4500, 5, 'accessories'),
(10, 't-shirt', 'blue mens t shirt.we have in all sizes Small, Medium ,Large', '0203209589BK-2_mens_t-Shirt_Fashion_Bug_Sri_Lanka.jpg', 2000, 5, 'gents'),
(14, 't-shirt', 'Mens white t shirt.we have in all sizes Small, Medium ,Large', '0203209589WHT-3_mens_t-Shirt_Fashion_Bug_Sri_Lanka.jpg', 2000, 5, 'gents'),
(16, 'Blue Top', 'blue color top.we have in all sizes Small, Medium ,Large', 'OIP.jfif', 1800, 5, 'ladies'),
(17, 'kids denim outfit', 'kids denim outfit shirt and bottom .we have in all sizes Small, Medium ,Large', 'FW2021DondupKids.jpg', 3500, 5, 'kids'),
(18, 'Frock', 'party frock colorful with all sizes small ,medium ,large.', 'OIP (1).jfif', 3500, 5, 'ladies');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
