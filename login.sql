-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 01, 2022 at 07:35 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daisywardrobr`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `cus_id` int(20) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telephone` int(11) NOT NULL,
  `DOB` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`cus_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`cus_id`, `fname`, `lname`, `email`, `telephone`, `DOB`, `password`) VALUES
(1, 'Ashinka', 'Meepagala', 'ashinka@gmail.com', 777725152, 'aug/21/2001', 'ash1234'),
(2, 'Shavin', 'Meepagala', 'shavin@gmail.com', 77725251, 'march/8/2001', 'ash12345'),
(3, 'John', 'wick', 'john@gmail.com', 777767123, 'may/8/2001', 'john123'),
(6, 'smith', 'damian', 'smith@gmail.com', 997745678, 'jan/12/2001', 'smith1234'),
(7, 'lakshan', 'widyapathi', 'lakshan@gmail.com', 779988213, 'sept/21/2001', 'lucky123'),
(8, 'Nimesh', 'dasan', 'Nimesh@gmail.cm', 772097627, 'may/07/1995', 'nimesh123'),
(9, 'Imesha', 'sawindi', 'imesha@gmail.com', 772065339, 'nov/23/1999', 'imesha123');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
